import gi
import platform
from time import sleep

gi.require_version("Gst", '1.0')

from gi.repository import Gst, GLib
from threading import Thread

Gst.init(None)

main_loop = GLib.MainLoop()
thread = Thread(target=main_loop.run)
thread.start()

video_sources = {
    "Windows": "ksvideosrc",
    "macOS": "autovideosrc",
    "Linux": "v4l2src"
}

system = platform.system()
pipeline = Gst.parse_launch(
    f"{video_sources[system]} ! decodebin ! videoconvert ! autovideosink"
)
pipeline.set_state(Gst.State.PLAYING)

try:
    while True:
        sleep(0.1)
except KeyboardInterrupt:
    pass

pipeline.set_state(Gst.State.NULL)
main_loop.quit()
thread.join()

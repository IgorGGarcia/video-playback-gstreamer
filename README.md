# Video Playback - GStreamer


## Summary
1. [Goal](#1-goal)
2. [Install dependencies](#2-install-dependencies)
3. [Getting started](#3-getting-started)

### 1. Goal
Create a simple video playback


### 2. Install dependencies

* Ubuntu, Debian
    ```
    sudo apt install libgstreamer1.0-0 gstreamer1.0-plugins-{base,good,bad,ugly} gstreamer1.0-tools python3-gi gir1.2-gstreamer-1.0
    ```
* Arch Linux, Manjaro
    ```
    sudo pacman -S gstreamer gst-plugins-{base,good,bad,ugly} python python-gobject
  ```
* macOS
    ```
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    ```
    ```
    brew install gstreamer gst-devtools gst-plugins-{base,good,bad,ugly} python@3 pygobject3
    ```
* Windows (Using MSYS2 MinGW 64-bit)
    ```
    pacman -S mingw-w64-x86_64-gstreamer mingw-w64-x86_64-gst-devtools mingw-w64-x86_64-gst-plugins-{base,good,bad,ugly} mingw-w64-x86_64-python3 mingw-w64-x86_64-python3-gobject
    ```

### 3. Getting started
In the terminal, type:

* Linux
    ```
    python3 main.py
    ```

* Windows
    ```
    py main.py
    ```

It's expected that open the camera and show a new window with the image from the camera.